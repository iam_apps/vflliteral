# VFLLiteral 
---
## Introduction

VFLLiteral is a swift library to create compile-time checked VFL expressions. _What does that mean?_

## Examples

Each of the following are valid VFLLiteral expressions. __Note that there are zero strings in these examples.__

```swift
    
    NSLayoutConstraint.activate(H:[spanner[<=0~999]]-(0~999)-[button[0]])
    NSLayoutConstraint.activate(V:[button[>=50]])
    NSLayoutConstraint.activate(H:[button]-10-[textField])
    NSLayoutConstraint.activate(V:[spanner[==textField]])
    NSLayoutConstraint.activate(H:[spanner[>=textField,<=button]])
    NSLayoutConstraint.activate(V:|-[textField[0]]-[purpleBox[>=20]]-|)
    NSLayoutConstraint.activate(H:[purpleBox[<=0]]-0-[button[0]]-(0~999)-|)
    
    NSLayoutConstraint.create(V:[textField][spanner])
    NSLayoutConstraint.create(H:[button[>=100~20]])
    NSLayoutConstraint.create(H:|[button]|)
    NSLayoutConstraint.create(H:[button[>=70,<=100]])
    NSLayoutConstraint.create(V:[button[==70~750,<=100, >=40]])
    NSLayoutConstraint.create(V:|-50-[purpleBox]-(50)-|)
    NSLayoutConstraint.create(V:|-[button]-[textField]-|)
    NSLayoutConstraint.create(H:|-[purpleBox[>=20]]-|)
    NSLayoutConstraint.create(H:[button]-[textField])
    NSLayoutConstraint.create(H:|-[button]-[textField[0]]-[purpleBox[>=20]]-|)
    NSLayoutConstraint.create(V:[purpleBox[<=0]][button[0]]-(0~999)-|)
    NSLayoutConstraint.create(V:[spanner[>=20]]-(>=0~998)-|)
    
    NSLayoutConstraint.create(H:[purpleBox[<=0]]-[button[0]]-(0~999)-|,
                              V:[purpleBox][textField]) // H and V at the same time
```

VFLLiteral expressions directly contain `UIView` or `NSView` types, as well as numeric types. This means there is no need for a `views` dictionary or a `metrics` dictionary. 
The `create` methods generate and return an array of `NSLayoutConstraint` which have not been activated. The `activate` methods return the same value plus activating the constraints before returning. 

The `create` and `activate` method forms both accept an `options` array for `NSLayoutFormatOptions` \ `NSLayoutConstraint.FormatOptions`.

```swift
    NSLayoutConstraint.activate(V:[spanner[<=0~999]]-(0~999)-[button[0]], options: [.alignAllCenterY])
```

The remainder of the public API should not be considered, except academically. It should be regarded as an implementation detail and subject to change. 