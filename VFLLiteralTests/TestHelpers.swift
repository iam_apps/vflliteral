//
//  TestHelpers.swift
//  CVFLTests
//
//  Created by Alex Lynch on 1/29/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import Foundation
import XCTest
import VFLLiteral

private var alternator = 0
func test(_ comp: VFL, against vflString: String) {
	if alternator % 2 == 0 {
		NSLayoutConstraint.activate(V:comp)
		XCTAssertEqual(NSLayoutConstraint.debug(V:comp), "V:\(vflString)")
	} else {
		NSLayoutConstraint.activate(H:comp)
		XCTAssertEqual(NSLayoutConstraint.debug(H:comp), "H:\(vflString)")
	}
	alternator += 1
}
