//
//  VFLLiteralTests.swift
//  VFLLiteralTests
//
//  Created by Alex Lynch on 1/30/18.
//  Copyright © 2018 TripFiles. All rights reserved.
//

import XCTest
import VFLLiteral

class VFLLiteralTests: XCTestCase {
	let superView = UIView()
	
	let spanner = UIView()
	let button = UIView()
	let textField = UIView()
	let purpleBox = UIView()
	
	override func setUp() {
		super.setUp()
		for view in [self.spanner, self.button, self.textField, self.purpleBox] {
			superView.addSubview(view)
		}
	}
	
	override func tearDown() {
		for view in superView.subviews {
			view.removeFromSuperview()
		}
		super.tearDown()
	}
	
	func testVLFEquivalence() {
		test([spanner[<=0~999]]-(0~999)-[button[0]], against: "[view0(<=0@999)]-(0@999)-[view1(0)]")
		test([spanner[>=20]]-(>=0~998)-|, against: "[view0(>=20)]-(>=0@998)-|")
		test([button]-[textField], against: "[view0]-[view1]")
		test([button[>=50]], against: "[view0(>=50)]")
		test(|[button]|, against: "|[view0]|")
		test(|-50-[purpleBox]-(50)-|, against: "|-(50)-[view0]-(50)-|")
		test([button]-10-[textField], against: "[view0]-(10)-[view1]")
		test([textField][spanner], against: "[view0][view1]")
		test([button[>=100.2~20]], against: "[view0(>=100.2@20)]")
		test([spanner[==textField]], against: "[view0(==view1)]")
		test([button[>=70,<=100]], against: "[view0(>=70,<=100)]")
		test([button[==70~750,<=100, >=40]], against: "[view0(70@750,<=100,>=40)]")
		test([spanner[>=textField,<=button]], against: "[view0(>=view1,<=view2)]")
		test(|-[button]-[textField]-|, against: "|-[view0]-[view1]-|")
		test(|-[purpleBox[>=20.5]]-|, against: "|-[view0(>=20.5)]-|")
		test(|-[textField[0]]-[purpleBox[>=20]]-|, against: "|-[view0(0)]-[view1(>=20)]-|")
		test(|-[button]-[textField[0]]-[purpleBox[>=20]]-|, against: "|-[view0]-[view1(0)]-[view2(>=20)]-|")
		test(|-0.5-[button[<=2.5]]-3.5-[textField[0.5~750]]-10.5-[purpleBox[>=20.5]]-0.5-|,
			 against: "|-(0.5)-[view0(<=2.5)]-(3.5)-[view1(0.5@750)]-(10.5)-[view2(>=20.5)]-(0.5)-|")
		test([purpleBox[<=0]][button[0]]-(0~999)-|, against: "[view0(<=0)][view1(0)]-(0@999)-|")
		test([purpleBox[<=0]]-0-[button[0]]-(0~999)-|, against: "[view0(<=0)]-(0)-[view1(0)]-(0@999)-|")
		test([purpleBox[<=0]]-[button[0]]-(0~999)-|, against: "[view0(<=0)]-[view1(0)]-(0@999)-|")
		test([button[100]]-50-|, against: "[view0(100)]-(50)-|")
		test(|-50-[button[100]], against: "|-(50)-[view0(100)]")
	}
	
	func testCGFloatSupport() {
		let mvDiameter: CGFloat = 36
		let _ = NSLayoutConstraint.create(H: [button[mvDiameter]])
	}
	
	func testDoubleVariableInterpolation() {
		let flag = false
		let gap = flag ? 0.0 : 15.0
		test([button]-gap-[purpleBox], against: "[view0]-(15)-[view1]")
		test([button][spanner]-gap-[purpleBox], against: "[view0][view1]-(15)-[view2]")
	}

	func testCGFloatVariableInterpolation() {
		let flag = false
		let gap:CGFloat = flag ? 0.0 : 15.0
		test([button]-gap-[purpleBox], against: "[view0]-(15)-[view1]")
		test([button][spanner]-gap-[purpleBox], against: "[view0][view1]-(15)-[view2]")
	}
	
	func testIntVariableInterpolation() {
		let flag = false
		let gap = Int(flag ? 0.0 : 15.0)
		test([button]-gap-[purpleBox], against: "[view0]-(15)-[view1]")
		test([button][spanner]-gap-[purpleBox], against: "[view0][view1]-(15)-[view2]")
	}
	
	func testSeraialSubscripts() {
		test([button][spanner][purpleBox], against: "[view0][view1][view2]")
		test([button][spanner][purpleBox[3]], against: "[view0][view1][view2(3)]")
	}
}
