//
//  VFLLiteral.swift
//  VFLLiteral
//
//  Created by Alex Lynch on 1/30/18.
//  Copyright © 2018 TripFiles. All rights reserved.
//

import Foundation
#if os(iOS) || os(watchOS) || os(tvOS)
import UIKit
public typealias View = UIView
public typealias ConstraintOptions = NSLayoutFormatOptions
#elseif os(OSX)
import AppKit
public typealias View = NSView
public typealias ConstraintOptions = NSLayoutConstraint.FormatOptions
#endif


prefix operator >=
prefix operator <=
prefix operator ==

prefix operator |
prefix operator |-
postfix operator |
postfix operator -|

infix operator ~ : AdditionPrecedence

public func ~(lhs: Int, rhs: Int) -> DimensionRelation {
	return DimensionRelation(relation: .eq, dimension: Double(lhs), priority: rhs)
}

public func ~(lhs: Double, rhs: Int) -> DimensionRelation {
	return DimensionRelation(relation: .eq, dimension: lhs, priority: rhs)
}

public func ~(lhs: CGFloat, rhs: Int) -> DimensionRelation {
	return DimensionRelation(relation: .eq, dimension: Double(lhs), priority: rhs)
}


public func ~(lhs: DimensionRelation, rhs: Int) -> DimensionRelation {
	return DimensionRelation(relation: lhs.relation, dimension: lhs.dimension, priority: rhs)
}

public protocol VFLComponent {
	func describe(using vfl: inout VFL) -> String
}

fileprivate protocol ConcreteRelation : VFLComponent {
	func describe(using vfl: inout VFL, useParens: Bool) -> String
}

extension ConcreteRelation {
	public func describe(using vfl: inout VFL) -> String {
		return self.describe(using: &vfl, useParens: true)
	}
}

public protocol AbstractView : VFLComponent {}
extension View : AbstractView {}
extension ConcreteViewRelations: AbstractView {}


public struct VFL {
	private var namedViews = Dictionary<String, View>()
	fileprivate var rootComponent: VFLComponent
	
	fileprivate init(rootComponent: VFLComponent) {
		self.rootComponent = rootComponent
	}
	
	fileprivate mutating func name(for view: View) -> String {
		for (name, otherView) in self.namedViews {
			if otherView == view {
				return name
			}
		}
		// no match
		let newName = "view\(namedViews.count)"
		self.namedViews[newName] = view
		return newName
	}
	
	fileprivate mutating func completeVFLString(with axis: String) -> String {
		return "\(axis):\(self.rootComponent.describe(using: &self))"
	}
	
	fileprivate mutating func createConstraints(with axis: String, options: ConstraintOptions) -> [NSLayoutConstraint] {
		let finalString = self.completeVFLString(with: axis)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: finalString, options: options, metrics: nil, views: self.namedViews)
		return constraints
	}
	
	public static postfix func -|(vfl: VFL) -> VFL {
		let comp = AdjacentComponents(dashed: true, firstViewRelation: vfl.rootComponent, secondViewRelation: SuperViewComponent())
		return VFL(rootComponent: comp)
	}
	
	public static postfix func |(vfl: VFL) -> VFL {
		let comp = AdjacentComponents(dashed: false, firstViewRelation: vfl.rootComponent, secondViewRelation: SuperViewComponent())
		return VFL(rootComponent: comp)
	}
	
	public static prefix func |-(vfl: VFL) -> VFL {
		let comp = AdjacentComponents(dashed: true, firstViewRelation: SuperViewComponent(), secondViewRelation: vfl.rootComponent)
		return VFL(rootComponent: comp)
	}
	
	public static prefix func |(vfl: VFL) -> VFL {
		let comp = AdjacentComponents(dashed: false, firstViewRelation: SuperViewComponent(), secondViewRelation: vfl.rootComponent)
		return VFL(rootComponent: comp)
	}
	
	public subscript(view: AbstractView) -> VFL {
		let comp = AdjacentComponents(dashed: false, firstViewRelation: self.rootComponent, secondViewRelation: view)
		return VFL(rootComponent: comp)
	}
}

extension VFL: ExpressibleByArrayLiteral {
	public init(arrayLiteral elements: AbstractView...) {
		guard let first = elements.first, elements.count == 1 else {
			preconditionFailure("you can only put one view inside square brackets: []")
		}
		self.init(rootComponent: first)
	}
}


public enum Relation : String, VFLComponent {
	case lessThanOrEqual = "<="
	case greaterThanOrEqual = ">="
	case equal = "=="
	
	static let lte = Relation.lessThanOrEqual
	static let gte = Relation.greaterThanOrEqual
	static let eq = Relation.equal
	
	public func describe(using: inout VFL) -> String {
		return self.rawValue
	}
}

public struct DimensionRelation : ConcreteRelation {
	let relation: Relation
	let dimension: Double
	let priority: Int
	
	init(relation: Relation, dimension: Double) {
		self.init(relation: relation, dimension: dimension, priority: 1000)
	}
	
	init(relation: Relation, dimension: Double, priority: Int) {
		self.relation = relation
		self.dimension = dimension
		self.priority = priority
	}
	
	func describe(using vfl: inout VFL, useParens: Bool) -> String {
		let openingString = useParens ? "(" : ""
		let equalityString = self.relation == .eq ? "" : self.relation.describe(using: &vfl)
		let dimesionString = (self.dimension - self.dimension.rounded(.down)) > 0.0 ? "\(self.dimension)" : "\(Int(self.dimension))"
		let priorityString = self.priority == 1000 ? "" : "@\(self.priority)"
		let closingString = useParens ? ")" : ""
		
		return "\(openingString)\(equalityString)\(dimesionString)\(priorityString)\(closingString)"
	}
	
	public static postfix func -|(dimensionRelation: DimensionRelation) -> VFL {
		return VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: dimensionRelation, secondViewRelation: SuperViewComponent()))
	}
	
	public static prefix func |-(dimensionRelation: DimensionRelation) -> LeftBoundVFL {
		return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: SuperViewComponent(), secondViewRelation: dimensionRelation)))
	}
}

extension DimensionRelation : ExpressibleByFloatLiteral {
	public init(floatLiteral value: Float) {
		self.init(relation: .eq, dimension: Double(value))
	}
}

extension DimensionRelation : ExpressibleByIntegerLiteral {
	public init(integerLiteral value: Int) {
		self.init(relation: .eq, dimension: Double(value))
	}
}

public extension Double {
	public static prefix func >=(dimension: Double) -> DimensionRelation {
		return DimensionRelation(relation: .gte, dimension: dimension)
	}
	public static prefix func <=(dimension: Double) -> DimensionRelation {
		return DimensionRelation(relation: .lte, dimension: dimension)
	}
	public static prefix func ==(dimension: Double) -> DimensionRelation {
		return DimensionRelation(relation: .eq, dimension: dimension)
	}
	public static postfix func -|(dimension: Double) -> VFL {
		return DimensionRelation(relation: .eq, dimension: dimension)-|
	}
	public static prefix func |-(dimension: Double) -> LeftBoundVFL {
		return |-DimensionRelation(relation: .eq, dimension: dimension)
	}
}

public extension Int {
	public static prefix func >=(int: Int) -> DimensionRelation {
		return DimensionRelation(relation: .gte, dimension: Double(int))
	}
	public static prefix func <=(int: Int) -> DimensionRelation {
		return DimensionRelation(relation: .lte, dimension: Double(int))
	}
	public static prefix func ==(int: Int) -> DimensionRelation {
		return DimensionRelation(relation: .eq, dimension: Double(int))
	}
	public static postfix func -|(int: Int) -> VFL {
		return DimensionRelation(relation: .eq, dimension: Double(int))-|
	}
	public static prefix func |-(int: Int) -> LeftBoundVFL {
		return |-DimensionRelation(relation: .eq, dimension: Double(int))
	}
}

public extension CGFloat {
	public static prefix func >=(cgFloat: CGFloat) -> DimensionRelation {
		return DimensionRelation(relation: .gte, dimension: Double(cgFloat))
	}
	public static prefix func <=(cgFloat: CGFloat) -> DimensionRelation {
		return DimensionRelation(relation: .lte, dimension: Double(cgFloat))
	}
	public static prefix func ==(cgFloat: CGFloat) -> DimensionRelation {
		return DimensionRelation(relation: .eq, dimension: Double(cgFloat))
	}
	public static postfix func -|(cgFloat: CGFloat) -> VFL {
		return DimensionRelation(relation: .eq, dimension: Double(cgFloat))-|
	}
	public static prefix func |-(cgFloat: CGFloat) -> LeftBoundVFL {
		return |-DimensionRelation(relation: .eq, dimension: Double(cgFloat))
	}
}

public struct ViewRelation : ConcreteRelation {
	let relation: Relation
	let view: View
	
	public func describe(using vfl: inout VFL) -> String {
		return self.describe(using: &vfl, useParens: false)
	}
	
	func describe(using vfl: inout VFL, useParens: Bool) -> String {
		precondition(useParens == false)
		return "\(relation.describe(using: &vfl))\(vfl.name(for: self.view))"
	}
}

public struct ConcreteViewRelations  {
	fileprivate let view: View
	fileprivate let relations: [ConcreteRelation]
}
extension ConcreteViewRelations : VFLComponent {
	public func describe(using vfl: inout VFL) -> String {
		return "[\(vfl.name(for: self.view))(\(relations.map{ $0.describe(using: &vfl, useParens: false) }.joined(separator: ",")))]"
	}
}

public extension View {
	public subscript (dimension: Int) -> ConcreteViewRelations {
		return ConcreteViewRelations(view: self, relations: [DimensionRelation(relation: .eq, dimension: Double(dimension))])
	}
	
	public subscript (dimension: Double) -> ConcreteViewRelations {
		return ConcreteViewRelations(view: self, relations: [DimensionRelation(relation: .eq, dimension: dimension)])
	}
	
	public subscript (dimension: CGFloat) -> ConcreteViewRelations {
		return ConcreteViewRelations(view: self, relations: [DimensionRelation(relation: .eq, dimension: Double(dimension))])
	}
	
	public subscript (relations: DimensionRelation...) -> ConcreteViewRelations {
		return ConcreteViewRelations(view: self, relations: relations)
	}
	
	public subscript (viewRelations: ViewRelation...) -> ConcreteViewRelations {
		return ConcreteViewRelations(view: self, relations: viewRelations)
	}
	
	public static prefix func >=(view: View) -> ViewRelation {
		return ViewRelation(relation: .gte, view: view)
	}
	public static prefix func <=(view: View) -> ViewRelation {
		return ViewRelation(relation: .lte, view: view)
	}
	public static prefix func ==(view: View) -> ViewRelation {
		return ViewRelation(relation: .eq, view: view)
	}
}

extension View : VFLComponent {
	public func describe(using vfl: inout VFL) -> String {
		return "[\(vfl.name(for: self))]"
	}
}

public struct SuperViewComponent : VFLComponent {
	public func describe(using vfl: inout VFL) -> String {
		return "|"
	}
}

public struct AdjacentComponents {
	fileprivate let dashed: Bool
	fileprivate let firstViewRelation: VFLComponent
	fileprivate let secondViewRelation: VFLComponent
}

extension AdjacentComponents : VFLComponent {
	public func describe(using vfl: inout VFL) -> String {
		return firstViewRelation.describe(using: &vfl) +
			(self.dashed ? "-" : "") +
			secondViewRelation.describe(using: &vfl)
	}
}

public struct LeftBoundVFL {
	let vfl: VFL
}

extension Array where Element == AbstractView {
	public subscript (relations: ConcreteViewRelations) -> VFL {
		guard let first = self.first, self.count == 1 else {
			preconditionFailure("you can only put one view inside square brackets: []")
		}
		return VFL(rootComponent:  AdjacentComponents(dashed: false, firstViewRelation: first, secondViewRelation: relations))
	}
	
	public subscript (relations: View) -> VFL {
		guard let first = self.first, self.count == 1 else {
			preconditionFailure("you can only put one view inside square brackets: []")
		}
		return VFL(rootComponent:  AdjacentComponents(dashed: false, firstViewRelation: first, secondViewRelation: relations))
	}
}

public func -(lhs: [AbstractView], rhs: Int) -> LeftBoundVFL {
	guard let first = lhs.first, lhs.count == 1 else {
		preconditionFailure("you can only put one view inside square brackets: []")
	}
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: first, secondViewRelation: DimensionRelation(relation: .eq, dimension: Double(rhs)))))
}

public func -(lhs: [AbstractView], rhs: Double) -> LeftBoundVFL {
	guard let first = lhs.first, lhs.count == 1 else {
		preconditionFailure("you can only put one view inside square brackets: []")
	}
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: first, secondViewRelation: DimensionRelation(relation: .eq, dimension: rhs))))
}

public func -(lhs: [AbstractView], rhs: CGFloat) -> LeftBoundVFL {
	guard let first = lhs.first, lhs.count == 1 else {
		preconditionFailure("you can only put one view inside square brackets: []")
	}
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: first, secondViewRelation: DimensionRelation(relation: .eq, dimension: Double(rhs)))))
}

public func -(lhs: VFL, rhs: Double) -> LeftBoundVFL {
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: lhs.rootComponent, secondViewRelation: DimensionRelation(relation: .eq, dimension: rhs))))
}

public func -(lhs: VFL, rhs: Int) -> LeftBoundVFL {
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: lhs.rootComponent, secondViewRelation: DimensionRelation(relation: .eq, dimension: Double(rhs)))))
}

public func -(lhs: VFL, rhs: CGFloat) -> LeftBoundVFL {
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: lhs.rootComponent, secondViewRelation: DimensionRelation(relation: .eq, dimension: Double(rhs)))))
}

public func -(lhs: VFL, rhs:  DimensionRelation) -> LeftBoundVFL {
	return LeftBoundVFL(vfl: VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: lhs.rootComponent, secondViewRelation: rhs)))
}

public func -(lhs: VFL, rhs:  VFL) -> VFL {
	return VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: lhs.rootComponent, secondViewRelation: rhs.rootComponent))
}

public func -(lhs: LeftBoundVFL, rhs:  VFL) -> VFL {
	return VFL(rootComponent: AdjacentComponents(dashed: true, firstViewRelation: lhs.vfl.rootComponent, secondViewRelation: rhs.rootComponent))
}

extension NSLayoutConstraint {
	/// Creates an Array of `NSLayoutConstriant`s corresponding to the VFL expression. The constraints are not activated.
	///
	/// - Parameters:
	///   - vfl: a VFL expression
	///   - options: an optional Array of `NSLayoutFormatOptions` or `NSLayoutConstraint.FormatOptions` depending on your platform.
	/// - Returns: an Array of `NSLayoutConstraints`
	open static func create(H vfl: VFL, options: ConstraintOptions = []) -> [NSLayoutConstraint] {
		var vfl = vfl
		return vfl.createConstraints(with: "H", options: options)
	}
	
	/// Creates an Array of `NSLayoutConstriant`s corresponding to the VFL expression and activates them.
	///
	/// - Parameters:
	///   - vfl: a VFL expression
	///   - options: an optional Array of `NSLayoutFormatOptions` or `NSLayoutConstraint.FormatOptions` depending on your platform.
	/// - Returns: an Array of `NSLayoutConstraints`
	@discardableResult
	open static func activate(H vfl: VFL, options: ConstraintOptions = []) -> [NSLayoutConstraint] {
		let constraints = self.create(H: vfl, options: options)
		NSLayoutConstraint.activate(constraints)
		return constraints
	}
	
	public static func debug(H vfl: VFL) -> String {
		var vfl = vfl
		return vfl.completeVFLString(with: "H")
	}
	
	/// Creates an Array of `NSLayoutConstriant`s corresponding to the VFL expression. The constraints are not activated.
	///
	/// - Parameters:
	///   - vfl: a VFL expression
	///   - options: an optional Array of `NSLayoutFormatOptions` or `NSLayoutConstraint.FormatOptions` depending on your platform.
	/// - Returns: an Array of `NSLayoutConstraints`
	open static func create(V vfl: VFL, options: ConstraintOptions = []) -> [NSLayoutConstraint] {
		var vfl = vfl
		return vfl.createConstraints(with: "V", options: options)
	}
	
	/// Creates an Array of `NSLayoutConstriant`s corresponding to the VFL expression and activates them.
	///
	/// - Parameters:
	///   - vfl: a VFL expression
	///   - options: an optional Array of `NSLayoutFormatOptions` or `NSLayoutConstraint.FormatOptions` depending on your platform.
	/// - Returns: an Array of `NSLayoutConstraints`
	@discardableResult
	open static func activate(V vfl: VFL, options: ConstraintOptions = []) -> [NSLayoutConstraint] {
		let constraints = self.create(V: vfl, options: options)
		NSLayoutConstraint.activate(constraints)
		return constraints
	}
	
	public static func debug(V vfl: VFL) -> String {
		var vfl = vfl
		return vfl.completeVFLString(with: "V")
	}
	
	/// Creates an Array of `NSLayoutConstriant`s corresponding to the sum of the two VFL expressions. The constraints are not activated.
	///
	/// - Parameters:
	///   - vfl: a VFL expression
	///   - options: an optional Array of `NSLayoutFormatOptions` or `NSLayoutConstraint.FormatOptions` depending on your platform.
	/// - Returns: an Array of `NSLayoutConstraints`
	open static func create(H h: VFL, V v: VFL) -> [NSLayoutConstraint] {
		var v = v
		var h = h
		
		return h.createConstraints(with: "H", options: []) + v.createConstraints(with: "V", options: [])
	}
	
	/// Creates an Array of `NSLayoutConstriant`s corresponding to the sum of the two VFL expressions and activates them.
	///
	/// - Parameters:
	///   - vfl: a VFL expression
	///   - options: an optional Array of `NSLayoutFormatOptions` or `NSLayoutConstraint.FormatOptions` depending on your platform.
	/// - Returns: an Array of `NSLayoutConstraints`
	@discardableResult
	open static func activate(H h: VFL, V v: VFL) -> [NSLayoutConstraint] {
		let constraints = self.create(H: h, V: v
		)
		NSLayoutConstraint.activate(constraints)
		return constraints
	}
}
