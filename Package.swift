// swift-tools-version:4.0
import PackageDescription

let package = Package(
	name: "VFLLiteral",
	targets: [
		.target(name: "VFLLiteral",
				dependencies: [],
				path: "VFLLiteral/"),
		.testTarget(name: "VFLLiteralTests",
					dependencies: ["VFLLiteral"],
					path: "VFLLiteralTests/"),
		.testTarget(name: "VFLLiteralGeneratedTests",
					dependencies: ["VFLLiteral"],
					path: "VFLLiteralGeneratedTests/"),
		]
)
