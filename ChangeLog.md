
# Change Log
---

### 
 * [8d6971b](../../commit/8d6971b) - __(Alex Lynch)__ Reorganize examples.
 * [8ee5c76](../../commit/8ee5c76) - __(Alex Lynch)__ Mark as v1.6.1
 * [f11ef0e](../../commit/f11ef0e) - __(Alex Lynch)__ Implement two missing use cases.
 * [7fcf322](../../commit/7fcf322) - __(Alex Lynch)__ Mark as v1.6
 * [17bef64](../../commit/17bef64) - __(Alex Lynch)__ Support for CGFloat dimensions.
 * [26f864e](../../commit/26f864e) - __(Alex Lynch)__ Mark as v1.5
 * [a4b28d0](../../commit/a4b28d0) - __(Alex Lynch)__ Updates for swift 4.1
 * [61a8d3a](../../commit/61a8d3a) - __(Alex Lynch)__ Mark as v1.4.1
 * [bbc38af](../../commit/bbc38af) - __(Alex Lynch)__ Support some VLF expression found wanting in the wild.
 * [8122066](../../commit/8122066) - __(Alex Lynch)__ Mark as v1.4
 * [2f6cf9c](../../commit/2f6cf9c) - __(Alex Lynch)__ Support non-integer dimensions.
 * [9c3a866](../../commit/9c3a866) - __(Alex Lynch)__ Implement beta SPM support.
 * [2655506](../../commit/2655506) - __(Alex Lynch)__ Mark as v1.2
 * [1c35269](../../commit/1c35269) - __(Alex Lynch)__ Use the `open` access modifier to remove the documentation of `public` implementation details.
 * [0a152b6](../../commit/0a152b6) - __(Alex Lynch)__ Mark as v1.1.3
 * [2dfb9a0](../../commit/2dfb9a0) - __(Alex Lynch)__ Update README.md
 * [27baaad](../../commit/27baaad) - __(Alex Lynch)__ Mark as v1.1.2
 * [0bf437b](../../commit/0bf437b) - __(Alex Lynch)__ Initial README.md file.
 * [8c42534](../../commit/8c42534) - __(Alex Lynch)__ Mark as v1.1.1
 * [809fdfa](../../commit/809fdfa) - __(Alex Lynch)__ Support macOS naming of `NSLayoutConstraint.FormatOptions`
 * [f12c94a](../../commit/f12c94a) - __(Alex Lynch)__ Mark as v1.1
 * [f17f8a8](../../commit/f17f8a8) - __(Alex Lynch)__ Implement NSLayoutOptions parameter to create and activate.
 * [1177030](../../commit/1177030) - __(Alex Lynch)__ Mark as v1.0.1
 * [bc40ddb](../../commit/bc40ddb) - __(Alex Lynch)__ Add license file.
 * [037a956](../../commit/037a956) - __(Alex Lynch)__ Mark as v1.0
 * [c43bbe5](../../commit/c43bbe5) - __(Alex Lynch)__ Support NSView.
 * [96a1d92](../../commit/96a1d92) - __(Alex Lynch)__ Mark as v0.9
 * [547d04e](../../commit/547d04e) - __(Alex Lynch)__ Mark as v0.9
 * [ff0d4d5](../../commit/ff0d4d5) - __(Alex Lynch)__ Mark as v0.9
 * [a2caa3d](../../commit/a2caa3d) - __(Alex Lynch)__ Mark as v0.9
 * [87a3405](../../commit/87a3405) - __(Alex Lynch)__ Improve tests. Beta make_release file.
 * [08b136c](../../commit/08b136c) - __(Alex Lynch)__ Exclude dangling dimension relations.
 * [c09b68e](../../commit/c09b68e) - __(Alex Lynch)__ Mark as v0.9
 * [3024faa](../../commit/3024faa) - __(Alex Lynch)__ Mark as v0.9
 * [e4ef58d](../../commit/e4ef58d) - __(Alex Lynch)__ Rename project.
 * [17979c1](../../commit/17979c1) - __(Alex Lynch)__ Apparently complete syntax. Prepare for test generation.
 * [3cac5b6](../../commit/3cac5b6) - __(Alex Lynch)__ Just use structs. It's nicer.
 * [17afbca](../../commit/17afbca) - __(Alex Lynch)__ Remove unnecessary generics on VFL.
 * [7a0f561](../../commit/7a0f561) - __(Alex Lynch)__ Test fundamental correctness of generated VFLs. Code badly needs to be organized.
 * [7fe0d14](../../commit/7fe0d14) - __(Alex Lynch)__ Can build NSLayoutConstraints
 * [e4f602c](../../commit/e4f602c) - __(Alex Lynch)__ Support multiple view relations.
 * [d3f9704](../../commit/d3f9704) - __(Alex Lynch)__ Better multi relation support.
 * [35c722f](../../commit/35c722f) - __(Alex Lynch)__ Initial support for multiple relations.
 * [d0e38ad](../../commit/d0e38ad) - __(Alex Lynch)__ Implement ViewViewRelation
 * [3568a19](../../commit/3568a19) - __(Alex Lynch)__ Initial commit. Proof of concept.
 * [b7398ff](../../commit/b7398ff) - __(Alex Lynch)__ Initial Commit
