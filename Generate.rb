#!/usr/bin/env ruby

require 'enumerator'

def output_to_file_if_different(filename, contnet)
	output_file = File.join(File.dirname(__FILE__),filename)

	existing_contents = File.exists?(output_file) ? File.readlines(output_file).join() : ""

	if (existing_contents != contnet)
		File.open(output_file,"w") {|f| f.print contnet }
	end
end

class Array
	def **(arr2)
		self.flat_map { |elem1|
			arr2.map { |elem2|
				elem1+elem2
			}
		}
	end
end

def relations
	["0",">=0","<=0","==0","0~999",">=0~999","<=0~999","==0~999"]
end

def separators 
	["","-"] + relations.map { |e| "-(#{e})-"}
end

def walls
	["","|"]
end

def views(count)
	["[view#{ count }]"] + relations.map { |e| "[view#{ count }{#{e}}]"}
end

def vfls 
	all = walls**separators**views(0)**separators**views(1)**separators**walls
	return all.delete_if { |vfl|
		vfl =~ /^-|-$/
	}
end

slice_counter = 0
vfls.each_slice(500) { |vfls|
	slice_name = slice_counter.to_s.rjust(3,"0")
	class_name = "VFLLiteralGenerated#{slice_name}Tests"
	test_declarations = []

	vfls.each_with_index { |vfl, count|
		vfl_expression = 	vfl.gsub("{","[").gsub("}","]")
		vfl_string = 			vfl.gsub("{","(").gsub("}",")").gsub("~","@")
		vfl_string.gsub!("==0","0")
		test_declarations << "func test#{count.to_s(16)}() {"
		test_declarations << "\ttest(#{vfl_expression}, against: \"#{vfl_string}\")"
		test_declarations << "}"
		test_declarations << ""
	}

	tests_string = %[
import XCTest
import VFLLiteral		
import UIKit
		
/// Generated tests
class #{class_name} : XCTestCase {
	
let superView = UIView()

let view0 = UIView()
let view1 = UIView()
	
override func setUp() {
	super.setUp()
	for view in [self.view0, self.view1] {
		superView.addSubview(view)
	}
}

override func tearDown() {
	for view in superView.subviews {
		view.removeFromSuperview()
	}
	super.tearDown()
}

#{test_declarations.join("\n")}
	
}
]

	output_to_file_if_different("VFLLiteralGeneratedTests/Generated/#{class_name}.swift",tests_string)

	slice_counter+=1
}


	