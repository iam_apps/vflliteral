
Pod::Spec.new do |s|
  s.name         = "VFLLiteral"
  s.version      = "1.6.2"
  s.summary      = "A swift library to create compile-time checked VFL expressions"

  s.homepage     = "http://iamapps.net/VFLLiteral/1.6.2/docs/index.html"
	s.documentation_url = s.homepage

  s.license      = { :type=>"MIT" }

  s.author       = { "Alex Lynch" => "alex@iamapps.net" }

  s.ios.deployment_target = "10.0"
  s.osx.deployment_target = "10.11"
  s.tvos.deployment_target = "10.0"
  s.pod_target_xcconfig = { "SWIFT_VERSION" => "4.1" }

  s.source       = { :git => "https://bitbucket.org/iam_apps/vflliteral.git", :tag=>"v1.6.2"}

  s.source_files = "VFLLiteral/*.swift"
end	
