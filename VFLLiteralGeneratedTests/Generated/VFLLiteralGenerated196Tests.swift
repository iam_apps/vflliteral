
import XCTest
import VFLLiteral		
import UIKit
		
/// Generated tests
class VFLLiteralGenerated196Tests : XCTestCase {
	
let superView = UIView()

let view0 = UIView()
let view1 = UIView()
	
override func setUp() {
	super.setUp()
	for view in [self.view0, self.view1] {
		superView.addSubview(view)
	}
}

override func tearDown() {
	for view in superView.subviews {
		view.removeFromSuperview()
	}
	super.tearDown()
}

func test0() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]|")
}

func test1() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-|")
}

func test2() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(0)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(0)-|")
}

func test3() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(>=0)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(>=0)-|")
}

func test4() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(<=0)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(<=0)-|")
}

func test5() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(==0)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(0)-|")
}

func test6() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(0~999)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(0@999)-|")
}

func test7() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(>=0~999)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(>=0@999)-|")
}

func test8() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(<=0~999)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(<=0@999)-|")
}

func test9() {
	test(|-(==0~999)-[view0[==0~999]]-(==0~999)-[view1[==0~999]]-(==0~999)-|, against: "|-(0@999)-[view0(0@999)]-(0@999)-[view1(0@999)]-(0@999)-|")
}

	
}
